const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
// const CompressionPlugin = require('compression-webpack-plugin');

module.exports = [
    {
        mode: 'production',
        devtool: 'source-map',
        target: 'web',
        entry: {
            client: path.join(__dirname, 'src', 'index.tsx'),
        },
        plugins: [
            new CleanWebpackPlugin({
                cleanOnceBeforeBuildPatterns: ['**/*', '!server*'],
            }),
            new HtmlWebpackPlugin({
                template: './src/pages/main-form.html',
                title: 'RushQA Test Zone',
                templateParameters: {
                    reactcdn: `<script
                    crossorigin
                    src="https://unpkg.com/react@17/umd/react.production.min.js"
                ></script>
                <script
                    crossorigin
                    src="https://unpkg.com/react-dom@17/umd/react-dom.production.min.js"
                ></script>`,
                },
            }),
            new MiniCssExtractPlugin({
                filename: '[name].css',
                chunkFilename: '[id].css',
            }),
            new ImageMinimizerPlugin({
                minimizerOptions: {
                    // Lossless optimization with custom option
                    plugins: [
                        ['gifsicle', { interlaced: true }],
                        ['jpegtran', { progressive: true }],
                        ['optipng', { optimizationLevel: 5 }],
                        [
                            'svgo',
                            {
                                plugins: [
                                    {
                                        removeViewBox: false,
                                    },
                                ],
                            },
                        ],
                    ],
                },
            }),
            // new CompressionPlugin(),
        ],
        output: {
            filename: '[name].[chunkhash:8].js',
            chunkFilename: '[name].[chunkhash:8].chunk.js',
            path: path.resolve(__dirname, 'build', 'prod'),
        },
        resolve: {
            extensions: ['.ts', '.tsx', '.js', '.jsx'],
            modules: [
                path.resolve(`${__dirname}/src`),
                path.resolve(`${__dirname}/node_modules`),
            ],
        },
        externals: {
            react: 'React',
            'react-dom': 'ReactDOM',
        },
        module: {
            strictExportPresence: true,
            rules: [
                {
                    test: /\.(ts|tsx|js|jsx)$/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            sourceType: 'unambiguous',
                        },
                    },
                    exclude: [
                        '/node_modules/',
                        /\bcore-js\b/,
                        /\bwebpack\/buildin\b/,
                        /@babel\/runtime-corejs3/,
                    ],
                },
                {
                    test: /\.css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 1,
                                modules: true,
                            },
                        },
                    ],
                    include: /\.module\.css$/,
                },
                {
                    test: /\.css$/,
                    use: [MiniCssExtractPlugin.loader, 'css-loader'],
                    exclude: /\.module\.css$/,
                },
                {
                    test: /\.s[ac]ss$/i,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                        },
                        {
                            loader: 'sass-loader',
                        },
                    ],
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    use: 'file-loader',
                },
            ],
        },
        optimization: {
            splitChunks: {
                cacheGroups: {
                    vendor: {
                        test: /node_modules/,
                        chunks: 'initial',
                        name: 'vendor',
                        priority: 10,
                        enforce: true,
                    },
                },
            },
            moduleIds: 'deterministic',
            runtimeChunk: 'single',
            minimize: true,
            minimizer: [
                new TerserPlugin({
                    parallel: true,
                }),
                new CssMinimizerPlugin(),
            ],
        },
    },
];
