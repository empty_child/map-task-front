import App from './app';
import config from './config';

(async () => {
    const app = App();
    const server = app.listen(config.port, () =>
        console.log(`Listening on port ${config.port}`));

    if (module.hot) {
        module.hot.accept(['./app'], () => {
            try {
                if (server.close) {
                    server.close(() =>
                        server.listen(config.port, () =>
                            console.log(
                                `[RESTART SERVER] Listening on port ${config.port}`,
                            )));
                }
            } catch (error) {
                console.log(
                    'Failed to update server. You probably need to restart the application',
                    error,
                );
            }
        });
    }
})();
