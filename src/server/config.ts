if (process.env.BROWSER) {
    throw new Error('Incorrect import `config.js`');
}

export default {
    port: process.env.PORT || 3000,

    // Authentication
    auth: {
        jwt: { secret: process.env.JWT_SECRET || 'test' },
    },
};
