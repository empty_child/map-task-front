/* eslint-disable no-unused-vars */
import express, { Request, Response } from 'express';
import helmet from 'helmet';
import path from 'path';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import React from 'react';
import ReactDOM from 'react-dom/server';
import chalk from 'chalk';
import cors from 'cors';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import ErrorPage from '../pages/error-page/error-page-without-styles';
import config from './config';

import { sequelize, User, Role } from '../models';

type ExpressError = Error & { status: number };

function App() {
    const app = express();

    function registerNodeMiddleware() {
        const corsOptions = {
            origin: 'http://localhost:8080',
        };

        app.use(cors(corsOptions));
        app.use(helmet());
        app.use('/assets', express.static(path.join(__dirname, './assets')));
        app.use(cookieParser());

        // parse requests of content-type - application/x-www-form-urlencoded
        app.use(bodyParser.urlencoded({ extended: true }));
        // parse requests of content-type - application/json
        app.use(bodyParser.json());
    }

    function dbInit() {
        function initial() {
            Role.create({
                id: 1,
                name: 'user',
            });

            Role.create({
                id: 2,
                name: 'moderator',
            });

            Role.create({
                id: 3,
                name: 'admin',
            });
        }

        sequelize.sync({ force: true }).then(() => {
            console.log('Drop and Resync Db');
            initial();
        });
    }

    function errorHandling() {
        app.use(
            // eslint-disable-next-line no-unused-vars
            (_req: Request, res: Response, _next: any) => {
                console.error(chalk.redBright('404'));
                const html = ReactDOM.renderToStaticMarkup(
                    <ErrorPage error="404" />,
                );
                res.status(400);
                res.send(`<!doctype html>${html}`);
            },
        );

        app.use(
            // eslint-disable-next-line no-unused-vars
            (err: ExpressError, _req: Request, res: Response, _next: any) => {
                console.error(chalk.redBright(String(err.status)));
                const html = ReactDOM.renderToStaticMarkup(
                    <ErrorPage error={String(err.status)} />,
                );
                res.status(err.status || 500);
                res.send(`<!doctype html>${html}`);
            },
        );
    }

    function registerUser() {
        app.post('/register', (req, res) => {
            const loginInfo = req.body;

            User.create({
                username: loginInfo.userName,
                email: loginInfo.email,
                password: bcrypt.hashSync(loginInfo.password, 10),
            })
                .then(() => res.sendStatus(200))
                .catch((err) => {
                    res.status(500).send({ message: err.message });
                });
        });
    }

    function authentificationInit() {
        app.post('/login', async (req, res) => {
            const loginInfo = req.body;
            const userInfo = {
                name: loginInfo.userName,
                password: loginInfo.password,
            };

            User.findOne({ where: { username: userInfo.name } })
                .then((user) => {
                    if (!user) {
                        return res
                            .status(404)
                            .send({ message: 'Пользователь не найден' });
                    }

                    const passwordIsValid = bcrypt.compareSync(
                        userInfo.password,
                        user.password,
                    );

                    if (!passwordIsValid) {
                        return res.status(401).send({
                            accessToken: null,
                            message: 'Неверный пароль!',
                        });
                    }

                    const token = jwt.sign(
                        { id: user.id },
                        config.auth.jwt.secret,
                        {
                            expiresIn: 86400, // 24 hours
                        },
                    );
                    return res.status(200).send({
                        id: user.id,
                        username: user.username,
                        email: user.email,
                        accessToken: token,
                    });
                })
                .catch((err) => {
                    res.status(500).send({ message: err.message });
                });
        });
    }

    function init() {
        registerNodeMiddleware();
        dbInit();
        registerUser();
        authentificationInit();
        errorHandling();
        return app;
    }

    return init();
}

export default App;
