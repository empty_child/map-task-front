import React, { PropsWithChildren } from 'react';
import { Container } from 'react-bootstrap';
import Header from 'components/header/header';
import Footer from 'components/footer/footer';

import './bootstrap-css-import.scss';
import styles from './app.module.css';

export default function App(props: PropsWithChildren<{}>) {
    const { children } = props;
    return (
        <>
            <div className="d-flex flex-column min-vh-100">
                <Header />
                <Container className={styles.mainContainer}>
                    {children}
                </Container>
                <Footer />
            </div>
        </>
    );
}
