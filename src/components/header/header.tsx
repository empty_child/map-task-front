import { getCurrentUser, logout } from 'api/login';
import React, { useEffect, useState } from 'react';
import {
    Nav, Navbar, Image, Form,
} from 'react-bootstrap';
import { Link } from 'react-router-dom';

import styles from './header.module.css';

export default function Header() {
    const [usernameState, setUsernameState] = useState('');

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {
        const { username } = getCurrentUser();
        setUsernameState(username);
    });
    return (
        <>
            <header>
                <Navbar className={styles.header} variant="light">
                    <Navbar.Brand as={Link} to="/">
                        <Image height="40px" src="/assets/logo.png" />
                    </Navbar.Brand>
                    <Navbar.Collapse className="justify-content-end">
                        <Nav>
                            {usernameState ? (
                                <>
                                    <Nav.Item>
                                        <Form inline>
                                            <Image
                                                className="border shadow-sm"
                                                height="30px"
                                                width="30px"
                                                src="/assets/favicon.ico"
                                                roundedCircle
                                            />
                                            <Nav.Link as={Link} to="/login">
                                                {usernameState}
                                            </Nav.Link>
                                        </Form>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link
                                            as={Link}
                                            to="/login"
                                            onClick={() => logout()}
                                        >
                                            Выйти
                                        </Nav.Link>
                                    </Nav.Item>
                                </>
                            ) : (
                                <Nav.Item>
                                    <Nav.Link as={Link} to="/login">
                                        Войти
                                    </Nav.Link>
                                </Nav.Item>
                            )}
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </header>
        </>
    );
}
