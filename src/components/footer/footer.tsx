import React from 'react';

import styles from './footer.module.css';

export default function Footer() {
    return (
        <>
            <footer className="footer border-top mt-auto">
                <p className={`text-muted text-center ${styles.footer} `}>
                    2020 - RushQA - Test Build
                </p>
            </footer>
        </>
    );
}
