import React from 'react';
import { Jumbotron } from 'react-bootstrap';

export default function FirstPage() {
    return (
        <>
            <Jumbotron>
                <h1>Добро пожаловать!</h1>
                <p>Это главная страница тестовой сборки. Развлекайтесь!</p>
            </Jumbotron>
        </>
    );
}
