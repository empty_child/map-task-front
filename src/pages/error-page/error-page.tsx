import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';

import styles from './error-page.module.css';

export default function ErrorPage({ error }: { error: string }) {
    return (
        <>
            <Container>
                <Row className={styles.test}>
                    <Col className="align-self-center">
                        <p className="text-center">{error}</p>
                    </Col>
                </Row>
            </Container>
        </>
    );
}
