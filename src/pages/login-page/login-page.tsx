import { loginUser, registerUser } from 'api/login';
import React, { FormEvent, useCallback, useState } from 'react';
import {
    Alert, Button, Col, Form, Image,
} from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

import styles from './login-page.module.css';

export default function LoginPage() {
    const [registerMode, setRegisterMode] = useState(false);
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');

    const [isRedirect, setIsRedirect] = useState(false);
    const [error, setError] = useState('');

    const logInUserOnSite = useCallback(async () => {
        const result = await loginUser(login, password);
        if (result.status === 200) {
            setIsRedirect(true);
        } else {
            setError(result.data.message || 'Неизвестная ошибка');
        }
    }, [login, password]);

    const registerUserOnSite = useCallback(async () => {
        const result = await registerUser(email, login, password);
        if (result.status === 200) {
            await logInUserOnSite();
            setIsRedirect(true);
        } else {
            setError(result.data.message || 'Неизвестная ошибка');
        }
    }, [email, logInUserOnSite, login, password]);

    return isRedirect ? (
        <Redirect to="/" />
    ) : (
        <>
            <Form
                validated={
                    registerMode
                        ? !!login && !!password && !!email
                        : !!login && !!password
                }
                className={`form-signin ${styles.loginPage}`}
            >
                <Form.Row className="text-center">
                    <Col />
                    <Col sm={8}>
                        <Form.Group>
                            <Button
                                variant="link"
                                disabled={!registerMode}
                                onClick={() => setRegisterMode(false)}
                            >
                                Войти
                            </Button>
                            {' '}
                            /
                            {' '}
                            <Button
                                variant="link"
                                disabled={registerMode}
                                onClick={() => setRegisterMode(true)}
                            >
                                Зарегистрироваться
                            </Button>
                        </Form.Group>
                        {registerMode ? (
                            <>
                                <Form.Group>
                                    <Image
                                        height="40px"
                                        src="/assets/logo.png"
                                    />
                                    <h3 className="display-3">
                                        Зарегистрироваться
                                    </h3>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Control
                                        placeholder="Почта"
                                        id="inputEmail"
                                        type="email"
                                        aria-describedby="почта"
                                        required
                                        value={email}
                                        onInput={(
                                            e: FormEvent<HTMLInputElement>,
                                        ) => setEmail(e.currentTarget.value)}
                                        autoFocus
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Control
                                        placeholder="Логин"
                                        id="inputLogin"
                                        aria-describedby="логин"
                                        value={login}
                                        onInput={(
                                            e: FormEvent<HTMLInputElement>,
                                        ) => setLogin(e.currentTarget.value)}
                                        required
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Control
                                        placeholder="Пароль"
                                        type="password"
                                        id="inputPassword"
                                        aria-describedby="пароль"
                                        value={password}
                                        onInput={(
                                            e: FormEvent<HTMLInputElement>,
                                        ) => setPassword(e.currentTarget.value)}
                                        required
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Button
                                        type="submit"
                                        onClick={() =>
                                            !!login
                                            && !!password
                                            && !!email
                                            && registerUserOnSite()}
                                    >
                                        Зарегистрироваться
                                    </Button>
                                </Form.Group>
                            </>
                        ) : (
                            <>
                                <Image height="40px" src="/assets/logo.png" />
                                <h3 className="display-3">Войти</h3>
                                {error && (
                                    <Alert variant="warning">{error}</Alert>
                                )}
                                <Form.Group>
                                    <Form.Control
                                        placeholder="Логин"
                                        id="inputLogin"
                                        aria-describedby="логин"
                                        value={login}
                                        onInput={(
                                            e: FormEvent<HTMLInputElement>,
                                        ) => setLogin(e.currentTarget.value)}
                                        required
                                        autoFocus
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Control
                                        placeholder="Пароль"
                                        type="password"
                                        id="inputPassword"
                                        aria-describedby="пароль"
                                        value={password}
                                        onInput={(
                                            e: FormEvent<HTMLInputElement>,
                                        ) => setPassword(e.currentTarget.value)}
                                        required
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Button
                                        onClick={() =>
                                            !!login
                                            && !!password
                                            && logInUserOnSite()}
                                    >
                                        Войти
                                    </Button>
                                </Form.Group>
                            </>
                        )}
                    </Col>
                    <Col />
                </Form.Row>
            </Form>
        </>
    );
}
