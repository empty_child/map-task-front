import { DataTypes, Sequelize } from 'sequelize';
import { UserStatic } from 'types/db';

export default (sequelize: Sequelize) => {
    const User = <UserStatic>sequelize.define('users', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        username: {
            type: DataTypes.STRING,
        },
        email: {
            type: DataTypes.STRING,
        },
        password: {
            type: DataTypes.STRING,
        },
    });

    return User;
};
