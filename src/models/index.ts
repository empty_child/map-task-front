import { Sequelize, Op } from 'sequelize';

import UserModel from './user.model';
import RoleModel from './role.model';

export const sequelize = new Sequelize('sqlite:database.sqlite', {
    operatorsAliases: Op,
    define: {
        freezeTableName: true,
    },
});

export const User = UserModel(sequelize);
export const Role = RoleModel(sequelize);

Role.belongsToMany(User, {
    through: 'user_roles',
    foreignKey: 'roleId',
    otherKey: 'userId',
});

User.belongsToMany(Role, {
    through: 'user_roles',
    foreignKey: 'userId',
    otherKey: 'roleId',
    as: 'roles',
});

export const ROLES = ['user', 'admin', 'moderator'];
