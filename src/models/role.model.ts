import { DataTypes, Sequelize } from 'sequelize';

export default (sequelize: Sequelize) => {
    const Role = sequelize.define('roles', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        name: {
            type: DataTypes.STRING,
        },
    });

    return Role;
};
