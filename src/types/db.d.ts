/* eslint-disable max-classes-per-file */
/* eslint-disable no-unused-vars */
import { BuildOptions, Model } from 'sequelize';

export interface UserAttributes {
    id?: number;
    username: string;
    email: string;
    password: string;
    createdAt?: Date;
    updatedAt?: Date;
}
export interface UserModel extends Model<UserAttributes>, UserAttributes {}
export class User extends Model<UserModel, UserAttributes> {}
export type UserStatic = typeof Model & {
    new (values?: object, options?: BuildOptions): UserModel;
};

export interface RoleAttributes {
    id: string;
    name: string;
    createdAt?: Date;
    updatedAt?: Date;
}
export interface RoleModel extends Model<RoleAttributes>, RoleAttributes {}
export class Role extends Model<RoleModel, RoleAttributes> {}
export type RoleStatic = typeof Model & {
    new (values?: object, options?: BuildOptions): RoleModel;
};
