import React, { createContext, useReducer } from 'react';
import combineReducers from './combine-reducers';
import UserReducer, { UserAction } from './reducers/user';

export type initialStateType = {
    [x: string]: any;
    user: {
        username: string;
    };
};

export const initialState: initialStateType = {
    user: { username: '' },
};

export type UnitedActions = UserAction;

export type Store = {
    state: initialStateType;
    dispatch?: React.Dispatch<UnitedActions>;
};

export const store = createContext<Store>({ state: initialState });
const { Provider } = store;

export function StateProvider({ children }: { children: any }) {
    const [state, dispatch] = useReducer(
        combineReducers({
            user: UserReducer,
        }),
        initialState,
    );
    return <Provider value={{ state, dispatch }}>{children}</Provider>;
}
