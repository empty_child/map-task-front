import { initialState, UnitedActions } from './store';

export default function combineReducers(reducers: any) {
    return (state = initialState, action: UnitedActions) => {
        const newState = initialState;
        Object.keys(reducers).forEach((elem) => {
            newState[elem] = reducers[elem](state[elem], action);
        });
        return newState;
    };
}
