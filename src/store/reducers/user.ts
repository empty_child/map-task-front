/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
import { initialStateType } from 'store/store';

export enum UserCases {
    SET_USERNAME = 'SET_USERNAME',
}
export type UserAction = { type: UserCases.SET_USERNAME } & { payload: string };

export default function UserReducer(
    state: initialStateType,
    action: UserAction,
) {
    switch (action.type) {
    case UserCases.SET_USERNAME:
        return {
            ...state.user,
            username: action.payload,
        };
    default:
        return state.user;
    }
}
