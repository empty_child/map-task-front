import React from 'react';
import { hydrate } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { BrowserRouter } from 'react-router-dom';
import routes from './routes';
import * as serviceWorkerRegistration from './service-worker-registration';

// eslint-disable-next-line no-undef
const IS_PRODUCTION = typeof NODE_ENV !== 'undefined' && NODE_ENV === 'production';

if (typeof window !== 'undefined') {
    if (!IS_PRODUCTION) {
        hydrate(
            <AppContainer>
                <BrowserRouter>{routes}</BrowserRouter>
            </AppContainer>,
            document.getElementById('app'),
        );
    } else {
        hydrate(
            <BrowserRouter>{routes}</BrowserRouter>,
            document.getElementById('app'),
        );
    }
}
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.unregister();
